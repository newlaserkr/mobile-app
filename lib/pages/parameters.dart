import 'dart:convert';

import 'package:devicelocale/devicelocale.dart';
import 'package:epine/main.dart';
import 'package:epine/model/camera.dart';
import 'package:epine/model/io.dart';
import 'package:epine/style/contants.dart';
import 'package:epine/style/fonts.dart';
import 'package:epine/utils/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_flutter/qr_flutter.dart';

class AddCameraWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StateAddCameraWidget();
}

class _StateAddCameraWidget extends State<AddCameraWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Camera camera = Camera();
  final WifiDetails wifiDetails = WifiDetails();

  @override
  void initState() {
    super.initState();
    setLocale();

    camera.key = generateKey();
    camera.id = getId(camera.key);
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  setLocale() async {
    try {
      Devicelocale.currentLocale.then((code) => wifiDetails.countryCode = code.toUpperCase());
    } on PlatformException {
      print("Error obtaining current locale");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Add Camera'), toolbarHeight: APP_BAR_HEIGHT),
      body: Form(
          key: _formKey,
          onChanged: () => {},
          //onWillPop: () => {},
          child: Column(children: <Widget>[
            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(10),
                child: Text("Basic Settings",
                    style: H2, textAlign: TextAlign.right)),
            TextParameterWidget(labelText: "Camera Name",
              onSaved: (String val) => camera.name = val),
            TextParameterWidget(labelText: "Wifi name (SSID)",
                onSaved: (String val) => wifiDetails.ssid = val),
            TextParameterWidget(labelText: "Wifi Password",
                onSaved: (String val) => wifiDetails.password = val),
            Container(
                padding: EdgeInsets.all(10),
                child: OutlineButton(
                    color: Colors.white,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        camera.wifiId = AppState.of(context).wifiController.save(wifiDetails);
                        AppState.of(context).cameraController.save(camera);

                        showModalQRCode(context);
                      }
                    },
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Image.asset('assets/img/qrcode_black.png',
                                  width: 30)),
                          Text('Generate QRcode')
                        ]))),
          ])),
    );
  }

  showModalQRCode(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext cv) {
          return Container(
            height: 350,
            color: Colors.transparent,
            child: Container(
                padding: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0))),
                child: Column(
                  children: [
                    Text("Scan QRcode on your Épine camera", textAlign: TextAlign.center,),
                    QrImage(
                      data: buildMessage(),
                      version: QrVersions.auto,
                      size: 300,
                    ),
            ])),
          );
        });
  }

  String buildMessage() {
    var mes = [QRCodeMessage.ofKey(camera.key).toJson(),
      QRCodeMessage.ofWifi(wifiDetails).toJson()];
    return jsonEncode(mes);
  }
}


class TextParameterWidget extends StatelessWidget {
  final String labelText;
  final Function onSaved;

  TextParameterWidget({this.labelText, this.onSaved});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: TextFormField(
          onSaved: onSaved,
          decoration: InputDecoration(
              border: OutlineInputBorder(), labelText: labelText),
          autofocus: true,
          autovalidate: true,
          validator: (String val) =>
          val.isEmpty ? "Cannot be blank" : null,
        ));
  }
}
