import 'package:epine/style/contants.dart';
import 'package:epine/style/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_sequence_animator/image_sequence_animator.dart';

class MainCameraWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StateMainCameraWidget();
}

class StateMainCameraWidget extends State<MainCameraWidget> {
  int _selectiedIndex = 0;

  final List<Widget> bodies = [
    TimelapseWidget(),
    LiveWidget(),
    NotificationWidget()
  ];

  void onTapped(int index) {
    setState(() {
      _selectiedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final String id = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Camera $id'),
        toolbarHeight: APP_BAR_HEIGHT,
        actions: [
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => {},
            tooltip: 'config',
          )
        ],
      ),
      body: bodies[_selectiedIndex],
      bottomNavigationBar:
          BottomNavigationBar(items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.fast_forward),
          title: Text('Timelapse'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.play_circle_filled),
          title: Text('Live'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notification_important),
          title: Text('Notification'),
        ),
      ], onTap: onTapped, currentIndex: _selectiedIndex),
    );
  }
}

class TimelapseWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StateTimelapseWidget();
}

class StateTimelapseWidget extends State<TimelapseWidget> {
  ImageSequenceAnimatorState _animator;
  bool reverse = false;
  bool wasPlaying = false;

  void onReadyToPlay(ImageSequenceAnimatorState _imageSequenceAnimator) {
    this._animator = _imageSequenceAnimator;
  }

  void onPlaying(ImageSequenceAnimatorState _imageSequenceAnimator) {
    setState(() {});
  }

  bool isAnimating() {
    if (_animator != null && _animator.animationController != null) {
      return _animator.animationController.isAnimating;
    } else {
      return true;
    }
  }

  forward() {
    _animator.play();
    setState(() => reverse = false);
  }

  rewind() {
    _animator.rewind();
    setState(() => reverse = true);
  }

  pause() {
    _animator.pause();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Expanded(
          child: ImageSequenceAnimator(
              "assets/ImageSequence", "Frame_", 0, 5, "png", 60,
              fps: 10,
              isAutoPlay: true,
              isLooping: false,
              color: Colors.grey,
              onReadyToPlay: onReadyToPlay,
              onPlaying: onPlaying)),
      Row(
        children: [
          IconButton(
              onPressed: isAnimating() && reverse ? pause : rewind,
              icon: Icon(
                  isAnimating() && reverse ? Icons.pause : Icons.fast_rewind)),
          IconButton(
              onPressed: isAnimating() && !reverse ? pause : forward,
              icon: Icon(isAnimating() && !reverse
                  ? Icons.pause
                  : Icons.fast_forward)),
          Expanded(
              child: Slider(
            value: _animator?.animationController?.value ?? 0,
            min: _animator?.animationController?.lowerBound ?? 0,
            max: _animator?.animationController?.upperBound ?? 100,
            onChangeStart: (double value) {
              wasPlaying = isAnimating();
              _animator.pause();
            },
            onChanged: (double value) {
              _animator.skip(value);
            },
            onChangeEnd: (double value) {
              if (wasPlaying) _animator.play();
            },
          )),
        ],
      )
    ]);
  }
}

class LiveWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child: Text(
          "Live is coming soon",
          style: H1,
          textAlign: TextAlign.center,
        ));
  }
}

class NotificationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child: Text(
          "Notification is coming soon",
          style: H1,
          textAlign: TextAlign.center,
        ));
  }
}
