import 'package:epine/pages/camera.dart';
import 'package:epine/pages/home.dart';
import 'package:epine/pages/parameters.dart';
import 'package:epine/service/repository.dart';
import 'package:epine/service/web.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(AppState(cameraController: CameraController(),
      shotController: ShotController(),
      wifiController: WifiController(), child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Épine',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: "Noto Sans",
      ),
      home: HomeWidget(),
      routes: {
        '/add': (context) => AddCameraWidget(),
        '/camera': (context) => MainCameraWidget()
      },
    );
  }
}

class AppState extends InheritedWidget {

  final CameraController cameraController;
  final WifiController wifiController;
  final ShotController shotController;

  const AppState({Key key, @required this.cameraController,
    @required this.wifiController, @required this.shotController,
    @required Widget child}) : super(key: key, child: child);

  static AppState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppState>();
  }

  @override
  bool updateShouldNotify(AppState old) {
    return old.cameraController != cameraController
        && old.wifiController != wifiController
        && old.shotController != shotController;
  }
}