import 'package:epine/model/io.dart';

class CameraCardDTO {
  final String id;
  final String name;
  final DateTime date;
  final String snapshot;

  const CameraCardDTO({this.id, this.name, this.date, this.snapshot});
}

class Camera {
  String id;
  String name;
  String wifiId;
  String key;

  Camera({this.id, this.name, this.wifiId});


  @override
  String toString() {
    return "{name: $name, id: $id, key: $key, wifiId: $wifiId}";
  }
}