import 'dart:collection';

class WifiDetails {
  String ssid;
  String password;
  String countryCode;

  WifiDetails({this.ssid, this.password, this.countryCode});

  @override
  String toString() {
    return "{ssid: $ssid, password: $password, countryCode: $countryCode}";
  }
}


class QRCodeMessage<T> {
  final String subject;
  final Map<String, T> content;

  const QRCodeMessage({this.subject, this.content});

  static QRCodeMessage<String> ofKey(String key) {
    return QRCodeMessage(subject: "encrypt_key",
        content: HashMap.fromIterables(['keys'], [key]));
  }

  static QRCodeMessage<String> ofWifi(WifiDetails wifi) {
    return QRCodeMessage(subject: "wifi",
        content: HashMap.fromIterables(['ssid', 'pwd', 'countryCode'], [wifi.ssid, wifi.password, wifi.countryCode]));
  }

  Map<String, dynamic> toJson() {
    return {"subject": subject, "content": content};
  }
}