import 'package:epine/main.dart';
import 'package:epine/model/camera.dart';
import 'package:epine/style/contants.dart';
import 'package:epine/style/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Épine'),
          toolbarHeight: APP_BAR_HEIGHT,
          actions: [
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () => Navigator.of(context).pushNamed("/add"),
              tooltip: 'add new camera',
            )
          ],
        ),
        backgroundColor: BACKGROUND,
        body: CameraListWidget());
  }
}

class CameraListWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StateCameraListWidget();
}

class _StateCameraListWidget extends State<CameraListWidget> {
  static MediaQueryData mqd;
  List<Camera> cameras;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    cameras = AppState.of(context).cameraController.getAll();
  }

  @override
  Widget build(BuildContext context) {
    cameras = AppState.of(context).cameraController.getAll();
    mqd = MediaQuery.of(context);

    return ListView(
      children: cameras.map(_buildItem).toList(),
    );
  }

  Widget _buildItem(Camera camera) {
    CameraCardDTO card = AppState.of(context).shotController.getCard(camera);
    return Container(
        padding: EdgeInsets.only(top: 10, bottom: 10),
        child: Card(
            child: GestureDetector(
              onTap: () => Navigator.of(context).pushNamed('/camera', arguments: camera.id),
          child: Column(children: [
            ListTile(
                title: Text(card.name, style: H2),
                subtitle: Text(card.date.toIso8601String(), style: H3)),
            Container(
                height: mqd.size.height * .25,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: AssetImage(card.snapshot),
                  ),
                ))
          ]),
        )));
  }
}
