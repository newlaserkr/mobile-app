import 'dart:math';

import 'package:epine/model/camera.dart';

class ShotController {
final Random r = Random.secure();


  CameraCardDTO getCard(Camera camera) {
    int i = r.nextInt(3);
    return CameraCardDTO(id: camera.id, name: camera.name, date: DateTime.now(), snapshot: "assets/img/test-$i.jpeg");
  }
}