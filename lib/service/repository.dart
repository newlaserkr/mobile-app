import 'package:epine/model/camera.dart';
import 'package:epine/model/io.dart';

class CameraController {
  Map<String, Camera> cameras = {
    "123": Camera(id: '123', name: 'Camera 1'),
    "234": Camera(id: '234', name: 'Camera 2')
  };

  List<Camera> getAll() => cameras.values.toList();

  String save(Camera camera) {
    cameras[camera.id] = camera;
    return camera.id;
  }

  delete(String id) {
    cameras.remove(id);
  }

}

class WifiController {
  Map<String, WifiDetails> wifis = {};

  String save(WifiDetails wifi) {
    wifis[wifi.ssid] = wifi;
    return wifi.ssid;
  }

  bool exist(WifiDetails wifi) {
    return wifis.containsKey(wifi.ssid);
  }

  WifiDetails get(String wifiId) {
    return wifis[wifiId];
  }
}