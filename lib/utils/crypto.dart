import 'dart:convert';
import 'dart:math';
import 'package:crypto/crypto.dart';


String generateKey() {
  var r = Random.secure();
  List<int> key = List<int>.generate(32, (i) => r.nextInt(256));
  return base64.encode(key);
}

String getId(String key) {
  var bytes = base64.decode(key);
  return sha256.convert(bytes).toString();
}